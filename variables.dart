// Variable Declarations
int age = 27;
String name = "Steve";
double piValue = 24.1;
bool status = true;

// Type Casting
void main() {
  print(age);
  print(name);
  print(piValue);
  print(status);
  print(age.runtimeType);
  print(name.runtimeType);
  print(piValue.runtimeType);
  print(status.runtimeType);
  print(
    age.toString(),
  );
}
