// Condition Statements if, else, else if,

// if and else syntax

import 'dart:io';

// Find the Odd Or Even Number
void oddOrEven() {
  print("Enter the Number to Find the odd or Even:");
  String numberValue = stdin.readLineSync()!;
  if (int.parse(numberValue) % 2 == 0) {
    print("$numberValue is even Number");
  } else {
    print("$numberValue is odd Number");
  }
}

// Find the biggest of the three numbers

void biggestOfThreeNumbers() {
  print("Enter the First Number: ");
  String number1 = stdin.readLineSync()!;
  var numberOneValue = int.parse(number1);
  print("Enter the Second Number: ");
  String number2 = stdin.readLineSync()!;
  var numberTwoValue = int.parse(number2);
  print("Enter the Third Number: ");
  String number3 = stdin.readLineSync()!;
  var numberThreeValue = int.parse(number3);

  if (numberOneValue > numberTwoValue && numberOneValue > numberThreeValue) {
    print("$numberOneValue is the Biggest Number");
  } else if (numberTwoValue > numberThreeValue &&
      numberTwoValue > numberOneValue) {
    print("$numberTwoValue is the Biggest Number");
  } else {
    print("$numberThreeValue is the Biggest Number");
  }
}

void main() {
  oddOrEven();
  biggestOfThreeNumbers();
}
